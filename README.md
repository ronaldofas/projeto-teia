# ProjetoTeia

Esse projeto é desenvolvido como parte do processo de seleção Projeto Teia.

## Para testar

Para efetuar a execução local da aplicação siga os seguintes passos:

* Clone este repositório em uma pasta de sua preferência
* Instale o nodejs e npm mais recentes.
* Instale o Angular com a versão 16 com o seguinte comando `npm install @angular/cli@16`
* Após instalar o Angular entre na pasta do projeto clonado e execute o comando `npm install`
* Se tudo for feito conforme o roteiro basta executar o servidor local com o comando `ng serve`
* Abra um navegador e digite o endereço "http://localhost:4200" e você acessará a aplicação.

A estrutura da aplicação foi pensada para ser a mais simples possível como entrega de um MVP  “Minimum Viable Product” (“Produto Mínimo Viável”, em português).

Além da estrutura padrão de um projeto Angular dentro da pasta app criamos as seguintes pastas:

- componentes: nessa estrutura foram criados todos os componentes que compõe a aplicação
- models: possue as classes de modelagem de objetos de dados utilizados na aplicação
- services: possuem as classes de serviços onde as regras de negócio foram implantadas.

O Design aplicado foi o Material por ser de simples utilização e amplamente conhecido pelos usuários por ser um padrão de mercado desenvolvido pela Google.

A tela principal apresenta o menu da aplicação e automaticamente apresenta o componente de tabela.

O componente de tabela apresenta os dados dos objetos obtidos e possibilita a exibição de um modal com os detalhes do objeto.

No menu é possível visualizar o segundo componente Cards que exibirá os mesmos dados no formato de Card.

Em ambos os componentes forma aplicados a ferramenta de paginação de dados o que torna a exibição mais clara e concisa sem perder a possibilidade de analisar todos os dados disponíveis.

Também foi adicionado a ambos a possibilidade de filtrar os dados por id, seja por valores pares, ímpares ou por números primos.

No componente tabela adicionamos a possibilidade de ordenção dos dados por colunas visiveis tanto na ordem crescente quanto decrescente.

Espero que apreciem o trabalho.

Repositório oficial: https://gitlab.com/ronaldofas/projeto-teia
Página de produção: https://main--projeto-teia.netlify.app/
