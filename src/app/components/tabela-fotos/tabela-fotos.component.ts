import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { PhotosModel } from 'src/app/models/photos.model';
import { PhotosService } from 'src/app/services/photos.service';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import {
  MatPaginator,
  MatPaginatorModule,
  PageEvent,
} from '@angular/material/paginator';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { DetalheDialogComponent } from '../tabela-fotos/detalhe-dialog/detalhe-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CommonModule } from '@angular/common';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { MatSort, MatSortModule } from '@angular/material/sort';

@Component({
  selector: 'app-tabela-fotos',
  templateUrl: './tabela-fotos.component.html',
  styleUrls: ['./tabela-fotos.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    MatInputModule,
    MatOptionModule,
    MatSortModule,
  ],
})
export class TabelaFotosComponent implements OnInit, AfterViewInit {
  photos: PhotosModel[] = [];
  photosOriginal: PhotosModel[] = [];
  photosExibicao: PhotosModel[] = [];
  displayedColumns: string[] = ['id', 'title', 'url', 'action'];
  dataSource = new MatTableDataSource<PhotosModel>(this.photos);
  modelName: string = '';
  length: number = this.photosOriginal.length;
  pageIndex: number = 0;
  pageSize: number = 5;
  pageEvent!: PageEvent;
  nomeSelecionado = new FormControl('tabela', []);
  sortByColumn: keyof PhotosModel = 'id';
  sortDirection: 'asc' | 'desc' = 'asc';

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(
    private photosService: PhotosService,
    public dialog: MatDialog,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.changeDetectorRef.detectChanges();
    this.capturaPhotos();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  sortBy(property: keyof PhotosModel) {
    if (property === this.sortByColumn) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortByColumn = property;
      this.sortDirection = 'asc';
    }

    this.photos.sort((a, b) => {
      const modifier = this.sortDirection === 'asc' ? 1 : -1;
      if (a[property] < b[property]) return -1 * modifier;
      if (a[property] > b[property]) return 1 * modifier;
      return 0;
    });
    this.separaExibicao();
  }

  abrirDialogo(row: PhotosModel) {
    this.dialog.open(DetalheDialogComponent, { data: row });
  }

  aplicarFiltro() {
    if (this.modelName === 'option0') {
      this.limparFiltro();
    } else if (this.modelName === 'option1') {
      this.photos = this.photosOriginal.filter(
        (photo: PhotosModel) => photo.id % 2 === 0
      );
      this.separaExibicao();
    } else if (this.modelName === 'option2') {
      this.photos = this.photosOriginal.filter(
        (photo: PhotosModel) => photo.id % 2 !== 0
      );
      this.separaExibicao();
    } else if (this.modelName === 'option3') {
      this.photos = this.photosOriginal.filter((photo: PhotosModel) =>
        this.ePrimo(photo.id)
      );
      this.separaExibicao();
    }
  }

  aplicarFiltroOnSelectChange(event: any) {
    this.aplicarFiltro();
  }
  limparFiltro() {
    this.photos = this.photosOriginal;
    this.separaExibicao();
  }

  handlePageEvent(e: PageEvent) {
    this.pageEvent = e;
    this.length = e.length;
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.separaExibicao();
  }

  private ePrimo(numero: number) {
    // Se o número for menor ou igual a 1, ele não é primo
    if (numero <= 1) {
      return false;
    }

    // Loop para verificar se o número é divisível por qualquer número entre 2 e a raiz quadrada do número
    for (let i = 2; i <= Math.sqrt(numero); i++) {
      if (numero % i === 0) {
        return false;
      }
    }

    // Se o número não for divisível por nenhum número entre 2 e a raiz quadrada do número, ele é primo
    return true;
  }

  private separaExibicao() {
    this.photosExibicao = this.photos.slice(
      this.pageIndex * this.pageSize,
      (this.pageIndex + 1) * this.pageSize
    );
    this.dataSource.data = this.photos;
    this.length = this.photos.length;
  }

  private capturaPhotos() {
    this.photosService.getPhotos().subscribe((photos: PhotosModel[]) => {
      this.photos = photos;
      this.photosOriginal = photos;
      this.dataSource.data = photos;
      this.separaExibicao();
    });
  }
}
