import { CommonModule } from '@angular/common';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import {
  MatDialog,
  MatDialogRef,
  MatDialogActions,
  MatDialogClose,
  MatDialogTitle,
  MatDialogContent,
  MatDialogModule,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { PhotosModel } from 'src/app/models/photos.model';

@Component({
  selector: 'app-detalhe-dialog',
  templateUrl: './detalhe-dialog.component.html',
  styleUrls: ['./detalhe-dialog.component.css'],
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatDialogModule, MatCardModule, MatIconModule]
})
export class DetalheDialogComponent implements OnInit {
  @Input() photo!: PhotosModel;

  constructor(@Inject(MAT_DIALOG_DATA) public data: PhotosModel) { }

  ngOnInit() {
    this.photo = this.data;
  }

  modificaLikes(card: PhotosModel) {
    card.likes = card.likes + 1;
  }

  modificaShares(card: PhotosModel) {
    card.shares = card.shares + 1;
  }
}
