import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import {
  MatPaginator,
  MatPaginatorModule,
  PageEvent,
} from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { PhotosModel } from 'src/app/models/photos.model';
import { MatIconRegistry, MatIconModule } from '@angular/material/icon';
import { PhotosService } from 'src/app/services/photos.service';
import { CommonModule } from '@angular/common';
import {
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cards-fotos',
  templateUrl: './cards-fotos.component.html',
  styleUrls: ['./cards-fotos.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
  ],
})
export class CardsFotosComponent implements OnInit, OnDestroy {
  photos: PhotosModel[] = [];
  photosOriginal: PhotosModel[] = [];
  photosExibicao: PhotosModel[] = [];
  dataSource: MatTableDataSource<PhotosModel> =
    new MatTableDataSource<PhotosModel>(this.photos);
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  length: number = this.photos.length;
  pageIndex: number = 0;
  pageSize: number = 5;
  pageEvent!: PageEvent;
  modelName: string = '';
  selectedName = new FormControl('', []);

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private photosService: PhotosService
  ) {}

  ngOnInit() {
    this.changeDetectorRef.detectChanges();
    this.dataSource.paginator = this.paginator;
    this.capturaPhotos();
  }

  ngOnDestroy(): void {}

  handlePageEvent(e: PageEvent) {
    this.pageEvent = e;
    this.length = e.length;
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.separaExibicao();
  }

  modificaLikes(card: PhotosModel) {
    card.likes = card.likes + 1;
  }

  modificaShares(card: PhotosModel) {
    card.shares = card.shares + 1;
  }

  exibeContagem(i: number) {
    console.log(i);
  }

  aplicarFiltro() {
    if (this.modelName === 'option0') {
      this.limparFiltro();
    } else if (this.modelName === 'option1') {
      this.photos = this.photosOriginal.filter(
        (photo: PhotosModel) => photo.id % 2 === 0
      );
      this.separaExibicao();
    } else if (this.modelName === 'option2') {
      this.photos = this.photosOriginal.filter(
        (photo: PhotosModel) => photo.id % 2 !== 0
      );
      this.separaExibicao();
    } else if (this.modelName === 'option3') {
      this.photos = this.photosOriginal.filter((photo: PhotosModel) =>
        this.ePrimo(photo.id)
      );
      this.separaExibicao();
    }
  }

  aplicarFiltroOnSelectChange(event: any) {
    this.aplicarFiltro();
  }
  limparFiltro() {
    this.photos = this.photosOriginal;
    this.separaExibicao();
  }

  private ePrimo(numero: number) {
    // Se o número for menor ou igual a 1, ele não é primo
    if (numero <= 1) {
      return false;
    }

    // Loop para verificar se o número é divisível por qualquer número entre 2 e a raiz quadrada do número
    for (let i = 2; i <= Math.sqrt(numero); i++) {
      if (numero % i === 0) {
        return false;
      }
    }

    // Se o número não for divisível por nenhum número entre 2 e a raiz quadrada do número, ele é primo
    return true;
  }

  private separaExibicao() {
    this.photosExibicao = this.photos.slice(
      this.pageIndex * this.pageSize,
      (this.pageIndex + 1) * this.pageSize
    );
    this.length = this.photos.length;
  }

  private capturaPhotos() {
    this.photosService.getPhotos().subscribe((photos: PhotosModel[]) => {
      this.photos = photos;
      this.photosOriginal = photos;
      this.dataSource.data = photos;
      this.separaExibicao();
    });
  }
}
