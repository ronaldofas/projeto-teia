export class PhotosModel {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
  likes: number = 0;
  shares: number = 0;

  constructor(
    albumId: number, id: number, title: string, url: string, thumbnailUrl: string,
    likes: number, shares: number) {
      this.albumId = albumId;
      this.id = id;
      this.title = title;
      this.url = url;
      this.thumbnailUrl = thumbnailUrl;
      this.likes = likes;
      this.shares = shares;
    }
}
