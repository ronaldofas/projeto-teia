import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { PhotosModel } from '../models/photos.model';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private http: HttpClient) { }

  getPhotos(): Observable<PhotosModel[]> {
    return this.http.get<PhotosModel[]>('https://jsonplaceholder.typicode.com/photos').pipe(
      map((photos: PhotosModel[]) => {
        return photos.map((photos: PhotosModel) => {
          return new PhotosModel(
            photos.albumId, photos.id, photos.title, photos.url, photos.thumbnailUrl, 0, 0);
          })
      })
    );
  }
}
