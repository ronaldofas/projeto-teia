import { ApplicationConfig, importProvidersFrom } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { provideAnimations } from "@angular/platform-browser/animations";
import { MAT_DIALOG_DEFAULT_OPTIONS } from "@angular/material/dialog";
import { RouterModule, Routes, provideRouter } from "@angular/router";
import { TabelaFotosComponent } from "./components/tabela-fotos/tabela-fotos.component";
import { CardsFotosComponent } from "./components/cards-fotos/cards-fotos.component";

const routes: Routes = [
  { path: 'table-view', component: TabelaFotosComponent },
  { path: 'card-view', component: CardsFotosComponent },
  { path: '', redirectTo: '/table-view', pathMatch: 'full' },
];

export const appConfig: ApplicationConfig = {
  providers: [
    importProvidersFrom(HttpClientModule),
    provideAnimations(),
    provideAnimations(),
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}},
    provideRouter(routes),
]
}
