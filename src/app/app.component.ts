import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabelaFotosComponent } from './components/tabela-fotos/tabela-fotos.component';
import { CardsFotosComponent } from './components/cards-fotos/cards-fotos.component';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, map } from 'rxjs';
import { HomeComponent } from './components/home/home.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    TabelaFotosComponent,
    CardsFotosComponent,
    HomeComponent,
  ],
})
export class AppComponent {
  title = 'projeto-teia';
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map((result) => result.matches));

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit() {}
}
